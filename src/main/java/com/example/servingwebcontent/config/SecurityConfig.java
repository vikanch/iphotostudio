package com.example.servingwebcontent.config;

import com.example.servingwebcontent.controllers.db.UsersController;
import com.example.servingwebcontent.models.Users;
import com.example.servingwebcontent.repository.UsersRepos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import com.example.servingwebcontent.config.custom.CustomUserDetailsService;
import javax.sql.DataSource;

// конфигурация для авторизации HTTP запросов
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers("/","/single", "/services", "/about", "/contact").permitAll() // Разрешаем доступ к указанным страницам для всех
                        .requestMatchers("/work_day/**").authenticated() // Разрешаем доступ к странице work_day только для пользователей с ролью USER
                        .requestMatchers("/css/**", "/js/**","/fonts/**","/images/**","/scss/**").permitAll()// Разрешаем доступ к статическим ресурсам всем
                        .anyRequest().authenticated() // Все остальные запросы требуют аутентификации
                )
                .formLogin(formLogin -> formLogin
                        .loginPage("/login") // Указываем страницу с формой логина
                        .permitAll() // Разрешаем доступ к странице логина всем
                        .defaultSuccessUrl("/work_day", true) // Переадресация после успешной авторизации на страницу work_day
                );

        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            UserDetailsService userDetailsService,
            PasswordEncoder passwordEncoder) {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder);

        ProviderManager providerManager = new ProviderManager(authenticationProvider);
        providerManager.setEraseCredentialsAfterAuthentication(false);

        return providerManager;
    }

    @Bean
    public UserDetailsService userDetailsService(CustomUserDetailsService customUserDetailsService) {
        return customUserDetailsService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

}