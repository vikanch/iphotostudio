package com.example.servingwebcontent.config.custom;

import com.example.servingwebcontent.models.Users;
import com.example.servingwebcontent.repository.UsersRepos;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UsersRepos usersRepos;

    public CustomUserDetailsService(UsersRepos usersRepos) {
        this.usersRepos = usersRepos;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Users user = usersRepos.findByEmail(email); // Assuming you have a method findByEmail in your repository
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + email);
        }

        // You can customize the UserDetails implementation based on your Users entity
        return org.springframework.security.core.userdetails.User
                .withUsername(user.getEmail())
                .password(user.getPassword())
                .roles("USER")
                .build();
    }
}
