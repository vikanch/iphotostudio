package com.example.servingwebcontent.controllers.db;

import com.example.servingwebcontent.repository.EquipmentRepos;
import org.springframework.stereotype.Controller;

@Controller
public class EquipmentController {
    private final EquipmentRepos equipmentRepos;

    public EquipmentController(EquipmentRepos equipmentRepos) {
        this.equipmentRepos = equipmentRepos;
    }
}
