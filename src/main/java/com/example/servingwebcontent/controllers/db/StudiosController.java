package com.example.servingwebcontent.controllers.db;

import com.example.servingwebcontent.repository.StudiosRepos;
import org.springframework.stereotype.Controller;

@Controller
public class StudiosController {
    private final StudiosRepos studiosRepos;

    public StudiosController(StudiosRepos studiosRepos) {
        this.studiosRepos = studiosRepos;
    }
}
