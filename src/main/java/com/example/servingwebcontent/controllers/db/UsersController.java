package com.example.servingwebcontent.controllers.db;

import com.example.servingwebcontent.models.Users;
import com.example.servingwebcontent.repository.UsersRepos;
import org.springframework.stereotype.Controller;

@Controller
public class UsersController {
    private final UsersRepos usersRepos;

    public UsersController(UsersRepos usersRepos) {
        this.usersRepos = usersRepos;
    }

    public UsersRepos getUsersRepos() {
        return usersRepos;
    }

    public boolean addUser(String fname, String lname, String email, String phone, String password, Boolean type)  {
        Users users = new Users(fname, lname, email, phone, password, type);
        usersRepos.save(users);
        return true;
    }

    public String searchEmail (String email) {
        Iterable<Users> users = usersRepos.findAll();
        for (Users user : users) {
            if (user.getEmail().equals(email)) {
                return email;
            }
        }
        return "";
    }

    public String searchPassword (String password) {
        Iterable<Users> users = usersRepos.findAll();
        for (Users user : users) {
            if (user.getPassword().equals(password)) {
                return password;
            }
        }
        return "";
    }
}
