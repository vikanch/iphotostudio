package com.example.servingwebcontent.controllers.db;

import com.example.servingwebcontent.models.WorkDay;
import com.example.servingwebcontent.repository.WorkDayRepos;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WorkDayController {

    private final WorkDayRepos workDayRepos;

    public WorkDayController(WorkDayRepos workDayRepos) {
        this.workDayRepos = workDayRepos;
    }

    @GetMapping("/workdays")
    public List<WorkDay> getAllWorkDays() {
        return (List<WorkDay>) workDayRepos.findAll();
    }
}


