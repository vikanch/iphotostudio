package com.example.servingwebcontent.controllers.web;

import com.example.servingwebcontent.controllers.db.UsersController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthController {

    private UsersController usersController;

    // Внедрение зависимости через конструктор
    public AuthController(UsersController usersController) {
        this.usersController = usersController;
    }
    @GetMapping("/login")
    public String login() {
        return "login"; // Это возвращает шаблон страницы с формой входа
    }
}
