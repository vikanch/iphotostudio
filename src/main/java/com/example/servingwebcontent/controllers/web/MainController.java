package com.example.servingwebcontent.controllers.web;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MainController {

    @GetMapping("/")
    public String home() {
        return "index";
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/contact")
    public String contacts() {
        return "contact";
    }

    @GetMapping("/services")
    public String services() {
        return "services";
    }

    @GetMapping("/single")
    public String single() {
        return "single";
    }

    @GetMapping("/work_day")
    public String work_day() {
        return "work_day";
    }

    @PostMapping("/contact/processButton")
    public ResponseEntity<Map<String, Object>> processButton(@RequestParam String buttonValue) {
        Map<String, Object> response = new HashMap<>();
        try {
            // Логика обработки формы

            response.put("success", true);
            return ResponseEntity.ok().body(response);
        } catch (Exception e) {
            response.put("success", false);
            return ResponseEntity.status(500).body(response);
        }
    }

}
