package com.example.servingwebcontent.models;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
public class Photograph {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studios studio;

    @ManyToOne
    @JoinColumn(name = "equipment_id")
    private Equipment equipment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Studios getStudio() {
        return studio;
    }

    public void setStudio(Studios studio) {
        this.studio = studio;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
