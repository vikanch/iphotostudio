package com.example.servingwebcontent.models;

import jakarta.persistence.*;
import org.springframework.data.annotation.Id;

@Entity
public class WorkDay {

    @jakarta.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double price;
    private String day;

    @ManyToOne
    @JoinColumn(name = "administration")
    private Users administration;

    @ManyToOne
    @JoinColumn(name = "photograph_id")
    private Photograph photograph;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users user;

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Users getAdministration() {
        return administration;
    }

    public void setAdministration(Users administration) {
        this.administration = administration;
    }

    public Photograph getPhotograph() {
        return photograph;
    }

    public void setPhotograph(Photograph photograph) {
        this.photograph = photograph;
    }

}
