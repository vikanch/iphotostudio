package com.example.servingwebcontent.models;

public class WorkDayRequestDto {
    private String fname;
    private String lname;
    private String email;
    private String phone;
    private String selectedDate; // Предполагается, что это поле будет содержать дату
    private String selectedHall; // Предполагается, что это поле будет содержать выбранный зал
    private String comment;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(String selectedDate) {
        this.selectedDate = selectedDate;
    }

    public String getSelectedHall() {
        return selectedHall;
    }

    public void setSelectedHall(String selectedHall) {
        this.selectedHall = selectedHall;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
