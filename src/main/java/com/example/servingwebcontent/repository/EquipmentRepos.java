package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.models.Equipment;
import org.springframework.data.repository.CrudRepository;

public interface EquipmentRepos extends CrudRepository<Equipment, Long> {
}
