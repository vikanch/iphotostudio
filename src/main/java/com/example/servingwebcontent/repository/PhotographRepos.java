package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.models.Photograph;
import org.springframework.data.repository.CrudRepository;

public interface PhotographRepos extends CrudRepository<Photograph, Long>{
}
