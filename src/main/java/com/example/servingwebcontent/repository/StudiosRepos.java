package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.models.Studios;
import org.springframework.data.repository.CrudRepository;

public interface StudiosRepos extends CrudRepository<Studios, Long> {

}
