package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.models.Users;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepos extends CrudRepository<Users, Long> {
    Users findByEmail(String email);
}
