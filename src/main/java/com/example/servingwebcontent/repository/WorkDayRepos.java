package com.example.servingwebcontent.repository;

import com.example.servingwebcontent.models.WorkDay;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkDayRepos extends CrudRepository<WorkDay, Long> {

}
